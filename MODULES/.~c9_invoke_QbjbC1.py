'''
Filename:   statesHyperlynxPod.py
Author:     Nathan James
Date:       10/24/16

Description:
    Creates stateMachine class object
    Creates a state for each state of the
    hyperloop pod. Defines functions for
    the state transitions


'''


import time
from stateMachine import StateMachine
from pod import Pod


default_state_settings = {}

def zeroCalibration(apod, duration):
    zero = apod.readSensors("A","G")
    cnt = 1
    for i in xrange(duration):
        new = apod.readSensors("A","G")
        for j in xrange(zero): zero[j] *= cnt
        cnt += 1
        for j in zero:
            zero[j] += new[j]
            zero[j] /= cnt
        time.sleep(0.01)
    return zero

# def dataLog(data, file):
    #enter timestamp and data into file
    #add newline char

# def processLog(stmnt, file):
    #enter timestamp and statement into file
    #add newline char

def pod_initialization_state(apod, duration):
	dataCheck = apod.readSensors()
	#need to open data file
	#need to open log file
	#check sensor data stream
	
	#zero out accl and gyro
	apod.cali
	#begin logging data with timestamps
	#check brake motor and DF motors (through feedback sensors)


# def pod_standby_state(apod, duration):
    # continue logging data
    # wait for next state transition
    # should this just be a transition function?


def pod_acceleration_state(apod):
    # continuously log data until negative acceleration
    nsample = 5
    dwell = 2.0
    count = 0
    pod_accel = delt = ctime = 0.0



    while pod_accel != None:                    # while pod in acceleration
        while count < nsample:
            pod_accel += apod.getData()[0][0]   # continuously read data
            count += 1

        pod_accel /= count                      # calculate average
        count = 0

        # need to test this logic
        if pod_accel < 0:                       # when neg accl detected
            if delt == 0.0:                     # first neg accl
                delt = time.time()
            delt = time.time() - delt           # calc delta time

            if (delt + ctime) >= dwell:         # if dwell time reached
                pod_accel = None                 # exit condition
            else:
                ctime += delt                   # cumulative time

            delt = time.time()                  # set new time

        else:                                   # reset time tracking
            delt = ctime = 0.0



def pod_braking_state(apod):
    # continue logging data
    # calculate braking needed from distance and speed
    # adjust braking to desired deccel for speed and distance


def print_braking_state():
    print("BRAKING!!!")


# def pod_absolute_braking_state(apod):
    




if __name__ == "__main__":
    hyperlynx = Pod()
