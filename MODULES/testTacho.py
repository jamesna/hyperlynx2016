from podSensors import PodSensors
import time
from smbus import SMBus
import RPi.GPIO as GPIO
import struct
import binascii


hyperbus = SMBus(1)
arduino = 0x42
value = 0;
stuff = 0;
tachoTime = []
tachoVelo = []
tachoPosition = []
        
class testTacho():

    #def __init__(self):
        ### Added for Position and Tachometer Interrupt


    def getPosition(self):
        #When triggered by interrupt:  Record Timestamp - Initial time in a list.
        writeNumber(0x69, 0x05)
        value = readNumber(0x69)
        print value
            

    def getTachos(channel, value):
        #Tacho I2C Port:  0x42
                    
        #When triggered by interrupt:  Record I2C bytes
        #I2C bytes:  4Bytes - timestamp.  2 Bytes - Velo   2 Bytes Position
        #Store I2C data into 3 variables to be used by state machine
        hyperbus.write_byte(0x42,0x05)
        val = hyperbus.read_i2c_block_data(0x42, 0x02, 10)
        #stuff = struct.unpack("<LHH",val)
        #stuff = self.bytesToNumber(self, val)
        #stuff = '{0:08x}{1:08x}'.format(val[0],val[1],val[2],val[3])
        print 'Tachos!:'
        print val
        return val


    #def bytesToNumber(self,b):
    #    total = 0
    #    multiplier = 1
    #    for count in xrange(len(b)-1, -1, -1):
    #        byte = b[count]
    #        total += multiplier * byte
    #        multiplier *= 256
    #    return total

    #def writeNumber(device, value):
    #    hyperbus.write_byte(device,value)
    #    return -1


    #def readNumber(device):
        #bus.read_i2c_block_data(address, command, count)
        #address:  The device (arduino) to connect to.  Set on the Arduino
        #command:  Hex value represents a register address on the Arduino
        #count:    Hex value for number of bits to expect from Command.
        #val = hyperbus.read_block_data(device, 0x02, 8)
        #return val      


if __name__ == "__main__":
        tachos = testTacho()
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(21, GPIO.FALLING, callback=tachos.getTachos, bouncetime=5)
        GPIO.add_event_detect(20, GPIO.FALLING, callback=tachos.getTachos, bouncetime=15)


        while True:
                time.sleep(3)




