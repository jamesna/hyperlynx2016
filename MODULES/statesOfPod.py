'''
Filename:   statesHyperlynxPod.py
Author:     Nathan James
Date:       10/24/16

Description:
    Creates stateMachine class object
    Creates a state for each state of the
    hyperloop pod. Defines functions for
    the state transitions


'''

import time
from stateMachine import StateMachine
from pod import Pod




default_state_settings = {}

class StatesOfPod(Pod):
    def __init__(self,prefs=None):
        Pod.__init__(self,prefs)
        
    
    # def processLog(stmnt, file):
        #enter timestamp and statement into file
        #add newline char


###############################################################################
# Initializatoin State
###############################################################################
    def pod_initialization_state(self):
        cmd = None
        # check SOC of battery; set up try/except error checking
        self.soc = self.sensors.voltData(self.board.analog_read(self.settings['vMtr']))
        print("Battery is at %.2f %% " % self.soc)
        
        # setup try/except error checking
    	
    	while cmd != "no":
    	    print("Current Sensor Readings: \n")
    	    print(self.readSensors())
    	    cmd = input("Update sensors: ")
    	# print sensor data to screen more pretty
    	
    	# calibrating Down force motors
    	cmd = input("Are we using the Down Force Assembly? ")
    	if cmd.lower() == "yes":
    	    while cmd != "exit":
    	        print("Current DFA positions: Front " + str(self.frontDFA.getPos())
    	               + ", Rear " + str(self.rearDFA.getPos()) )
    	        try:
    	            cmd = input("Enter front position: ")
    	        except ValueError:
    	            print("Did not receive a value")
    	            continue
    	        self.frontDFA.movePos(cmd)
    	        
    	        try:
    	            cmd = input("Enter rear position: ")
    	        except ValueError:
    	            print("Did not receive a value")
    	            continue
    	        self.rearDFA.movePos(cmd)
    	        
    	# calibrating brake motors
    	cmd = input("Test brakes? ")
    	if cmd.lower() == "yes":
    	    while cmd != "exit":
    	        print("Current brake position: " + str(self.masterBrake.getPos()))
    	        try:
    	            cmd = input("Enter brake position: ")
    	        except ValueError:
    	            print("Did not receive a value")
    	            continue
    	        self.masterBrake.movePos(cmd)
    	    self.masterBrake.movePos(0)

    	                
    	cmd = None
    	
    	while cmd != "no":
    	    cmd = input("Ready to Calibrate Accelerometer")
    	    # zero out accl and gyro
    	    print("Calibrating Accelerometer...")
    	    zero = [self.signal_average(self, 10)]
    	    zero.append(self.signal_average(10,0,1))
    	    zero.append(self.signal_average(10,0,2))
    	    self.setZero(zero)
    	    print("...")
    	    print("Resting acceleration: \n")
    	    print(self.readSensors())
    	    cmd = input("Re-calibrate? ")
 
 ###############################################################################
 ###############################################################################
 ###############################################################################
 # Standby State
 ###############################################################################
    	
    def pod_standby_state(self):
        print("Recticulating Splines...")
        while self.acceleration < 0.2:
            self.acceleration = self.signal_average()
        # continue logging data
        # wait for next state transition
        # should this just be a transition function?
        return "acceleration"

###############################################################################
###############################################################################
# Acceleration State
###############################################################################
    
    def pod_acceleration_state(self, dwell = 2.0):
        # continuously log data until negative acceleration
        pod_accel = delt = ctime = 0.0
    
    
    
        while pod_accel != None:                    # while pod in acceleration
            pod_accel = self.signal_average(self, 5)
    
            # need to test this logic
            if pod_accel < 0:                       # when neg accl detected
                delt = time.time() - delt           # calc delta time
    
                if (delt + ctime) >= dwell:         # if dwell time reached
                    pod_accel = None                 # exit condition
                else:
                    ctime += delt                   # cumulative time
    
                delt = time.time()                  # set new time
    
            else:                                   # reset time tracking
                delt = ctime = 0.0

###############################################################################
###############################################################################
###############################################################################
# Braking State
###############################################################################
    
    
    # def pod_braking_state(self):
        # continue logging data
        # calculate braking needed from distance and speed
        # adjust braking to desired deccel for speed and distance
    
    
    def print_braking_state(self):
        print("BRAKING!!!")
    
    
    # def pod_absolute_braking_state(self):
    
###############################################################################
###############################################################################     
    
    def signal_average(self, nsample = 5, i = 0, j = 0):
        # continuously log data until negative acceleration
        count = 0
        average = 0.0
        if hasattr(self.readSensors()[i], '__iter__'):
            while count < nsample:
                average += self.readSensors()[i][j]   # continuously read data
                count += 1
        else:
            while count < nsample:
                average += self.readSensors()[i]   # continuously read data
                count += 1
    
        average /= count                      # calculate average
        count = 0
        return average





if __name__ == "__main__":
    import signal
    import sys
    
    def signal_handler(sig, frame):
        print('You pressed Ctrl+C')

        if hyperlynx.board is not None:
            hyperlynx.board.reset()
        sys.exit(0)
    
    hyperlynx = StatesOfPod()
    signal.signal(signal.SIGINT, signal_handler)
    
    hyperlynx.pod_initialization_state()
