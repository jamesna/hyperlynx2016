# Class for Controlling Linear Actauators through PyMata
# to Arduino for HyperLynx. A PyMata object is needed to
# be passed into the class to utilize this class
# Written by Nathan James and Bejan Akhavan
# 9/20/16 - modfied 10/8/16

from PyMata.pymata import PyMata
import time

class LinearActuator():

#########################################################
    def __init__(self, board, forward, reverse, position, settings=None):
        self.forward = forward          # pin for forward
        self.reverse = reverse          # pin for reverse
        self.position = position                  # pin for position
        self.board = board              # board object

        if settings == None:
            self.settings = {'spos':0.28,
                             'sanalog':2,
                             'fpos':1.73,
                             'fanalog':863,
                             'plmn':0.125
                            }
        
        
#########################################################
#########################################################


#########################################################
    def motorF(self, spd=255): #default speed is max
        if spd > 255:
           spd = 255
        elif spd < 0:
           spd = 0
        self.board.analog_write(self.forward, spd)
#########################################################
# motorF is Motor Forward method it takes input int
# from 0-255 and writes the PWM signal to the Arduino
# pin for forward
#########################################################


#########################################################
    def motorR(self, spd=255): #default speed is max
        if spd > 255:
           spd = 255
        elif spd < 0:
           spd = 0
        self.board.analog_write(self.reverse, spd)
#########################################################
# motorR is Motor Reverse method: it takes input int
# from 0-255 and writes the PWN signal to the Arduino
# pin for reverse
#########################################################


#########################################################
    def getPos(self):
        length = (self.settings['fpos'] - (self.settings['fanalog'] -
                self.board.analog_read(self.position)) *
                (self.settings['fpos'] - self.settings['spos'])/
                (self.settings['fanalog'] - self.settings['sanalog']))
        return length ##self.board.analog_read(self.position)
#########################################################
# getPos returns the position of the motor using the
# motor's potentiometer
#########################################################


#########################################################
    def movePos(self, pos, spd=255): # 255 is hex for 100% open
        current_position = self.getPos()
        old_position = None

        while (not(pos-self.settings['plmn'] <= current_position <= 
                pos+self.settings['plmn']) and current_position != old_position):
            if current_position < pos:
                self.motorF(spd)
                time.sleep(.2)
            elif current_position > pos:
                self.motorR(spd)
                time.sleep(.2)
            old_position = current_position
            current_position = self.getPos()
        self.stop()
#########################################################
# movePos moves the motor to the position pos passed into
# the function
#########################################################



#########################################################
    def stop(self):
        self.board.analog_write(self.forward, 0)
        self.board.analog_write(self.reverse, 0)

#########################################################
# stop method stops the motor in either direction
#########################################################




if __name__ == "__main__":
    board = PyMata("/dev/ttyACM0", verbose=True)
    board.set_pin_mode(9,board.PWM,board.DIGITAL)
    board.set_pin_mode(10,board.PWM,board.DIGITAL)
    board.set_pin_mode(0,board.INPUT,board.ANALOG)
    
    test = LinearActuator(board,9,10,0)
    
    print("Current Position:")
    print(test.getPos())
    
    position = input("Enter position to move to (inches):")
    test.movePos(position)
    
    while position >= 0:
        print("Current Position:")
        print(test.getPos())
        
        position = input("Enter position to move to (inches):")
        test.movePos(position)
    
    
    print("Exiting Now")
    test.board.close()