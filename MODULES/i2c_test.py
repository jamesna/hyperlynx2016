import smbus
import time
# for RPI version 1, use 'bus = smbus.SMBus(0)'

bus = smbus.SMBus(1)

# This is the address set up on the Arduino
add1 = 0x04
add2 = 0x05

def writeNumber(value,device):
    if device == 1:
        address = add1
    else:
        if device == 2:
            address = add2
        
    # bus.write_byte(address,value)
    bus.write_byte_data(address,0,value)
    # bus.write_block_data(address,
    return -1

def readNumber(device):
    if device == 1:
        address = add1
    else:
        if device == 2:
            address = add2
    
    # number = bus.read_byte(address)
    number = bus.read_byte_data(address,1)
    return number

while True:
    device = input("Choose an Arduino 1 or 2:")
    var = input("Enter 1 - 9:")
    if not var:
        continue

    writeNumber(var,device)
    print "RPI: Hi Arduino, I sent you ", var
    # sleep one second
    time.sleep(1)

    number=readNumber(device)
    print "Arduino: Hey RPI, I received a digit ", number
    
