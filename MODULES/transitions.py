'''
Filename:   transitions.py

Author:     Nathan James
Date:       01/14/17

Description: 
        Contains state machine transition functions for the HyperLynx pod
        state machine. These functions are used to transition between states
        when a pod state function has finished executing.
'''

from stateMachine import StateMachine
import statesHyperLynxPod as states

# text strings to describe failure states
pod_failure_adjectives = [ "navigation", "power", "overheat", 
                                "electro_mechanical", "communication"]
                                

# state transitions functions handle next state setting, depending on the txt string

def pod_standby_state_transitions(txt):
	splitted_txt = txt.split(None,1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	
	states.pod_standby_state()

	# only allowed transition is from initialization to standby
	if word == "initialization":
		newState = "pod_standby_state"
	else:
		newState = "error_state"
	return (newState, txt)



def pod_acceleration_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")

	# only allowed transition is from standby to acceleration
	if word == "standby":
		newState = "pod_acceleration_state"
	else:
		newState = "error_state"
	return (newState, txt)




def pod_braking_state_transitions(txt):
	splitted_txt = txt.split(None, 1)
	word, txt = splitted_txt if len(splitted_txt) > 1 else (txt, "")
	if word == "acceleration":
		newState = "pod_braking_state"
	else:
		newState = "error_state"
	return (newState, txt)





	
