% Script for reading in and cleaning data from Tachometer testing
clc;clf;clear all; close all

format short g

zero = [];

data = {500, 750, 1000, 1200, 1500, 1700, 2000};

data(2,1) = load('tachTest500.dat');
data(2,2) = load('tachTest750.dat');
data(2,3) = load('tachTest1000.dat');
data(2,4) = load('tachTest1200.dat');
data(2,5) = load('tachTest1500.dat');
data(2,6) = load('tachTest1700.dat');
data(2,7) = load('tachTest2000.dat');


%fstats = @(x1,x2,x3,x4,x5,x6,x7) [500, statistics(x1)'; 750, statistics(x2)'; 
%	1000, statistics(x3)'; 1200, statistics(x4)';
%	1500, statistics(x5)'; 1700, statistics(x6)';
%	2000, statistics(x7)'];
err = [];

for i = 1:7
	data(3,i) = statistics(data{2,i});
	for j = data{2,i}'
		if j < data{3,i}(6)+3*data{3,i}(7) && j > data{3,i}(6)-3*data{3,i}(7)
			zero(end+1) = j;
			err(end+1) = j/data{1,i};
		end
	end
	data(4,i) = zero';
	data(5,i) = err';
	zero = [];
	err = [];

	data(6,i) = statistics(data{5,i});
end

for i = 1:7
	err(end+1) = (data{6,i}(6) - 1) * 100;
	err = err';
end

data(7,1) = err;

plot(data{7,1})

figure(2)
for i = 1:7
	plot(data{4,i})
	hold on
end

