'''
Filename:   hyperlynx_master.py
Authors:    Sean Yu, Nathan James, Meng Xiong
Date:       07/29/2016

Description:
    University of Colorado Denver SpaceX HyperLoop 
    Team "HyperLynx" Pod Master Prototyping 
    Program, Python Version 2.7

ASSUMING MEALY MACHINE Architecture:

Major Phase States:

0000: Standby
0001: Pre-Flight
0010: Flight
0011: Post-Flight

Operational States:

0100: Primary Braking (Hydraulic Actuators)
0101: Auxiliary Braking (Linear Actuators)

Failure States/Shutdowns:

0110: Overheat shutdown
0111: (Tire Explosion/Failure?)
'''

from stateMachine import StateMachine



