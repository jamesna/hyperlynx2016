/*
 * Authors: Nathan James and John Gorzynski
 * Date:    1/17/16
 * File:    IR_Tachometer
 * 
 * Tachometer code using an IR proximity detector
 * 
 * 
 * Notes: This has to start at low RPS to correctly set the delay range.
 *        Possibly the times between interrupts could be used if they
 *        less than 1 ms while on detection section (simpler solution)
 */
 
#include <Wire.h>

#define SLAVE_ADDRESS 0x42    // For I2C
int number = 0;
int state = 0;


volatile unsigned short REV;       //  VOLATILE DATA TYPE TO STORE REVOLUTIONS
volatile unsigned long lastISR = 0;
//volatile unsigned short RPS_buff [20];
//volatile unsigned byte i = 0;

const byte tdelay = 7;     // delay timing [ms]
const float wheelD = 1.9271;     // wheel diameter [ft]

volatile unsigned short RPS = 0;  //  DEFINE RPS AND MAXIMUM RPS
volatile unsigned short velo = 0;
volatile unsigned short pos = 0;
unsigned long start_time;     // Define current time each loop   
unsigned long tstamp = 0;
boolean tacho_time, reset_arduino, send_data, active = false;

unsigned int line = 1;
 
//int ledPin = 13;           //   STATUS LED
 
//int led = 0;  //  INTEGERS TO STORE LED VALUE 
    
 void setup()
 {
     Serial.begin(115200);   // GET VALUES USING SERIAL MONITOR     
     attachInterrupt(digitalPinToInterrupt(2), RPSCount, LOW);     //  ADD A HIGH PRIORITY ACTION ( AN INTERRUPT)  WHEN THE SENSOR GOES FROM LOW TO HIGH
     
     REV = 0;      //  START ALL THE VARIABLES FROM 0     
     RPS = 0;     

    Wire.begin(SLAVE_ADDRESS);  // initialize i2c as slave

    Wire.onReceive(receiveData);  //I2C
    Wire.onRequest(sendData);     //I2C

     
//     pinMode(ledPin, OUTPUT);     
     pinMode(3, OUTPUT);                
     pinMode(4, OUTPUT);     
     digitalWrite(3, HIGH);             //  VCC PIN FOR SENSOR     
     digitalWrite(4, LOW);              // GND PIN FOR SENSOR
     
 }
 
 void loop()
 {
  if(active){
    start_time = millis();                 // GET CURRENT TIME    
  }
  if(tacho_time){
      digitalWrite(12, HIGH);
      delayMicroseconds(25);
      digitalWrite(12, LOW);  
      RPS = 1000/(millis() - lastISR);//  CALCULATE  RPS USING REVOLUTIONS AND ELAPSED TIME       
      tstamp = millis() - start_time;                            
      pos = 6.0214*REV;          //If this throws an error, include:  (unsigned int) 
      velo = 6.0214*RPS;
      tacho_time = false;
  }
   

  if (reset_arduino){
    //digitalWrite(10,HIGH); //Wired to reset pin
    start_time = 0;
    velo = 0;
    pos = 0;
    REV = 0;
    reset_arduino = false;
    tacho_time = false;
    active = false;
  }
}
 
 void RPSCount()                                // EVERYTIME WHEN THE SENSOR GOES FROM LOW TO HIGH , THIS FUNCTION WILL BE INVOKED 
 {
  if((millis() - lastISR) > tdelay)
  {
   REV++;                                       // INCREASE REVOLUTIONS
   //if(i>19)
   //   i = 0;
   //RPS_buff[i++] = millis();                    // Record when the revolution beings 
  }

  lastISR = millis();
  tacho_time = true;
 }
   
//callback for received I2C data
void receiveData(int byteCount){

  while(Wire.available()){
    number = Wire.read();
    //Serial.print("data received: ");
    //Serial.println(number);
        
    switch(number){
      case 0x00:
        reset_arduino = true;
        break; 
      case 0x01:
        active = true;
        break;
      case 0x05:
        send_data = true;
        break;
      case 0x09:
        testTacho();
        break;
    }
    Serial.print("data received: ");
    Serial.println(RPS);
    
  }
}

void sendData(){

if(!reset_arduino){
  byte data [10];
  data[0] = (tstamp >> 24) & 0xFF;
  data[1] = (tstamp >> 16) & 0xFF;
  data[2] = (tstamp >> 8) & 0xFF;
  data[3] = (tstamp & 0xFF);
  data[4] = velo >> 8;
  data[5] = (velo & 0xFF);
  data[6] = pos >> 8;
  data[7] = pos & 0xFF;
  data[8] = (REV >> 8) & 0xFF;
  data[9] = REV & 0xFF;
  byte qty = 0x0A;
  Wire.write(data,10);
  }
}

void testTacho(){
  tstamp = 0xFF010203;
  velo = 0x1111;
  pos = 0xAAAA;
  sendData();
  tstamp = 0;
  velo = 0;
  pos = 0;
}

 
//////////////////////////////////////////////////////////////  END OF THE PROGRAM  ///////////////////////////////////////////////////////////////////////
