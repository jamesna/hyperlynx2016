int pot = 0;
int fwd = 5;
int rev = 6;
int pwr = 75;

void setup() {
  pinMode(fwd,OUTPUT);
  pinMode(rev,OUTPUT);

}

void loop() {
  int sensorValue = analogRead(pot);

  if (sensorValue < 512){
    analogWrite(fwd, 0);
    analogWrite(rev, pwr);
    delay(5000);
  }
  else {
    analogWrite(fwd, pwr);
    analogWrite(rev, 0);
    delay(6100);
    
    analogWrite(fwd, 0);
    analogWrite(rev, pwr);
    delay(4400);
  }

}
