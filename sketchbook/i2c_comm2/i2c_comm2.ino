#include <Wire.h>

#define SLAVE_ADDRESS 0x05
int number = 0;
int state = 0;

unsigned short tstamp, diff;

void setup() {
//pinMode(13,OUTPUT);    //For testing LED
Serial.begin(115200);  //Start serial for output  
Wire.begin(SLAVE_ADDRESS);  // initialize i2c as slave

Wire.onReceive(receiveData);
Wire.onRequest(sendData);

Serial.println("#2 Ready!");
}

void loop() {
  if (reset_arduino){
    reset_arduino = false;
    digitalWrite(10,HIGH); //Wired to reset pin
  }
  // put your main code here, to run repeatedly:
  delay(1);
}

//callback for received data
void receiveData(int byteCount){

  while(Wire.available()){
    number = Wire.read();
    
    switch(number){
      case 0x00:
        reset_arduino= true;
        break; 
      case 0x05:
        start_tachos = true;
        break;
      case 0x0A:
        send_data = true;
        break;
    }
    
    Serial.print("data received #2: ");
    Serial.println(number);
    }
  }
}

//callback for sending data
void sendData(){
  data[0] = tstamp >> 8;
  data[1] = tstamp & 0xFF;
  data[2] = diff >> 8;
  data[3] = diff & 0xFF;
  byte qty = 0x06;
  Wire.write(number);
}

