int ird = 5;
int value = 0;
int sum = 0;
int count = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  while(count < 10){
    value = analogRead(ird);
    sum += value;
    count++;
    delay(10);
  }
  Serial.println(sum/count);
  sum = 0;
  count = 0;
}
