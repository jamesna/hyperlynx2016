int pcell = 0;
int light = 13;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(light, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:

  int sensorValue = analogRead(pcell);

  if (sensorValue > 200){
    digitalWrite(light,HIGH);
  }
  else {
    digitalWrite(light,LOW);
  }

  Serial.println(sensorValue);
  delay(10);
}
