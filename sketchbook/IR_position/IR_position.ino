
volatile unsigned int count;       //  VOLATILE DATA TYPE TO STORE REVOLUTIONS
 
unsigned long velocity;
unsigned long pos = 0;
bool interrupted = false;
 
unsigned long t1me, currentTime;         //  DEFINE TIME TAKEN TO COVER ONE REVOLUTION
 
int ledPin = 13;           //   STATUS LED
 
int led = 0;  //  INTEGERS TO STORE LED VALUE 
    
 void setup()
 {
     Serial.begin(9600);   // GET VALUES USING SERIAL MONITOR
     
     attachInterrupt(0, POSCount, RISING);     //  ADD A HIGH PRIORITY ACTION ( AN INTERRUPT)  WHEN THE SENSOR GOES FROM LOW TO HIGH
     
     count = 0;      //  START ALL THE VARIABLES FROM 0
        
     t1me = 0;
     
     pinMode(ledPin, OUTPUT);
     
//     pinMode(3, OUTPUT);           
     
     //pinMode(4, OUTPUT);
     
     //digitalWrite(3, HIGH);             //  VCC PIN FOR SENSOR
     
     //digitalWrite(4, LOW);              // GND PIN FOR SENSOR
     
 }
 
 void loop()
 {
  long currtime = millis();                 // GET CURRENT TIME
     
    if( interrupted )                               //  IT WILL UPDATE AFETR EVERY 5 READINGS
   { 
     velocity = (currtime - t1me)*count*100;       //  CALCULATE  RPM USING REVOLUTIONS AND ELAPSED TIME
     
     t1me = currtime;                            

    //JOHN:  Add offset of track when we get to LA    
     pos +=  count*100;         //If this throws an error, include:  (unsigned int) 
     
     //JOHN:  Fix this!
     //Serial.printf("RPM: %s  Velo: %s  Pos:  %s \n", rpm, velocity, pos);
     //Serial.println(rpm);

     
     
     //delay(500);
     interrupted = false;
     }
   

     Serial.print("Pin stat: ");                // check if sensor is detecting object
     Serial.println(digitalRead(2));
 }
 
 void POSCount()                                // EVERYTIME WHEN THE SENSOR GOES FROM LOW TO HIGH , THIS FUNCTION WILL BE INVOKED 
 {
                                              // INCREASE REVOLUTIONS
   if (currentTime < t1me+50.0){
        currentTime = t1me;
        count++;  
        interrupted = true;
         if (led == LOW)
         {
           
           led = HIGH;                                 //  TOGGLE STATUS LED
         } 
         
         else
         {
           led = LOW;
         }
         digitalWrite(ledPin, led);

    } 
   
 }
//////////////////////////////////////////////////////////////  END OF THE PROGRAM  ///////////////////////////////////////////////////////////////////////
