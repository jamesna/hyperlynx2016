// File:    SPI_Test
// Project: HyperLynx2016
// Author:  John Gorzynski
// Date:    1/21/2017

// Original Example by Nick Gammon
// http://raspberrypi.stackexchange.com/questions/44277/spi-raspberry-pi-master-and-arduino-slave

#include <SPI.h>

char buf [100];
volatile byte loc, command;
volatile boolean send_data, start_tacho;

volatile unsigned short t, velo, pos;
float accel;
unsigned short velo_buff [10], velo_avg;
char *out;


void setup (void)
{
  Serial.begin (115200);   // debugging

  // SPI: turn on SPI in slave mode
  SPCR |= _BV(SPE);
  SPCR |= _BV(SPIE);

  // SPI: have to send on MISO - master in, slave out
  //pinMode(MOSI, INPUT);
  pinMode(MISO, OUTPUT);
  //pinMode(10, INPUT);  //The Cable Select or SS pin
  
  //SPI.begin();
  //SPI.setBitOrder(MSBFIRST);
  
  // SPI: get ready for an interrupt
  loc = 0;  // buffer empty
  send_data = false;
  start_tacho = false;

  // SPI: now turn on interrupts
  SPI.attachInterrupt();

  Serial.println("Arduino SPI Test is ready");

} //end of setup

// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // grab byte from SPI Data Register

  switch(command)
  {
    case 0xAA:
      command = c;
      SPDR = 0;
      start_tacho = true;
      Serial.print("Tacho Time!");
      break;

    case 0x05:
      SPDR = c + 100;
      send_data = true;
      Serial.print("Send the Tachos");
      break;

    case 0xFF:
      SPDR = 0;
      send_data = false;
      //t = 0;
      velo = 0;
      pos = 0;
  }

  // add to buffer if theres room
  if (loc < sizeof(buf) -1)
  {
    buf[loc++] = c;
  
  
   /* if (c == 0xFF) {
      //Critical error - reboot arduino
    }
    if (c == 0x03){  //Pi sends 0xAA first to start the tachometer
      Serial.println("Initialize Tachometer");
      start_tacho = true;
    }
    if (c== 0x00){ //Pi sends 0x00 to reset values
      Serial.println("Reset Values!");
      t = 0;
      velo = 0;
      pos = 0;
    }*/
    
    //buf [loc++] = c;
    //Serial.println(c);

    // ex: newline means time to process buffer
    if (c == 0x05){
      send_data = true;
      Serial.print("Send Data is: ");
      Serial.println(send_data);
    }
  }
}  //end of interrupt routine SPI_STC_vect

// main loop - wait for flag set in interrupt routine
void loop (void)
{
  int i, c;
  //RPi sends 0xAA to start tacho measurements
  while(start_tacho){
    //Create test data based on time, velocity, and position
    //time: 0 to 15000 in test:  15 seconds
    //velo: 0 for 1000 (1 sec).  Accel 3g for 3 3000.  Decel -0.7G for 6000
    //pos:  use velo for rpm.  D*pi*rpm = position
    
    //Outer time loop
    accel = 0;
    velo = 0;
    pos = 0;
    for(t=0;t<4000;t++){
      
      if(t==500) {  //acceleration phase
        accel = .09645669;   // 3G in ft/ms:  29.4 m/s or 96.46 ft/s
      } 
      else if(t==2000){  //braking phase
        accel = -.0225065;   // -0.7G in ft/ms: 6.86 m/s or 22.5 ft/s
      }

        velo = accel*(t-500)*200; 
        pos = velo*(t-500) + 0.5*accel*(t-500)*(t-500);
 
        Serial.print(t);
        Serial.print("|");
        Serial.print(velo);
        Serial.print("|");
        Serial.println(pos);

    
    //RPi sends 0x00 every 5 milliseconds to request current measurements
    if (send_data)
    {
      Serial.print("Something!");
      for(i=0;i<10;i++){
        velo_avg += velo_buff[i];
      }
      sprintf(out, "%d|%d|%d\n", t, velo_avg, pos);
      velo_avg /= 10;
      
      //digitalWrite(10,LOW);
      //SPI.transfer(0);
      pos = 42;
      buf[loc] = 0;
      Serial.print("buf:");
      Serial.println(buf);
      Serial.print("pos:");
      Serial.println(pos);
      SPI.transfer(pos);
      //digitalWrite(10,HIGH);
      buf[loc]=0;
      //Serial.println(0x10);
      loc = 0;
      send_data = false; 
    }

    start_tacho = false;
    }
  }
  

}  //end of Main
