const int buttonRev = 13;
const int buttonFwd = 12;
const int ledFwd = 1;
const int ledRev = 0;
//const int pot = 0;
const int fwd = 9;
const int rev = 10;
const int pwr = 255;

int fwdState = 0;
int revState = 0;
int sensorValue = 0;

void setup() {
  pinMode(ledFwd, OUTPUT);
  pinMode(ledRev, OUTPUT);
  pinMode(fwd, OUTPUT);
  pinMode(rev, OUTPUT);
  pinMode(buttonFwd, INPUT);
  pinMode(buttonRev, INPUT);
  Serial.begin(9600);
}

void loop() {
  fwdState = digitalRead(buttonFwd);
  revState = digitalRead(buttonRev);

  if (revState == HIGH) {
    analogWrite(rev, pwr);
    digitalWrite(ledRev, HIGH);
  }
  else if (fwdState == HIGH){
    analogWrite(fwd, pwr);
    digitalWrite(ledFwd, HIGH);
  }
  else {
    analogWrite(fwd, 0);
    analogWrite(rev, 0);
    digitalWrite(ledFwd, LOW);
    digitalWrite(ledRev, LOW);
  }

  Serial.print(fwdState);
  Serial.println(revState);
}
