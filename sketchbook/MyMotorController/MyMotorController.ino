int RPWM_Output = 5; // Arduino PWM output pin 5
int LPWM_Output = 6; // Arduino PWN output pin 6


void setup()
{
  pinMode(RPWM_Output, OUTPUT);
  pinMode(LPWM_Output, OUTPUT);
}

void loop()
{
  analogWrite(LPWM_Output, 255);
  analogWrite(RPWM_Output, 90);
}
