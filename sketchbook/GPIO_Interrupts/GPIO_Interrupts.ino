void setup() {
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);

}

void loop() {
  while(1){
    for(int i = 0; i < 5; i++){
      
      //Set timer for pin 3 to fire every 20 milliseconds.  set to low after 10 microseconds
      //  Put this in a for loop so that it fires 100 times
      //  Outside the for loop fire Pin 5 once immediately, followed by set to low after 10 microseconds
      
      delay(2000);
      digitalWrite(3, HIGH);
      delay(2000);
      digitalWrite(3,LOW);
      delay(2000);
      digitalWrite(5, HIGH);
      delay(2000);
      digitalWrite(5, LOW);
    }
  }
  delay(45000);
}
