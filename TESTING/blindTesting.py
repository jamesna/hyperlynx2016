'''
Filename:   blindTesting.py
Author:     Nathan James
Date:       10/26/16

Description:
    Runs a blind testing program for when network access is
    unavailable.This program will setup all the sensors and
    controls and run through a calibration to set
    acceleration and gyroscopic movement to zero over a 5
    second period. It will then wait for a 2 second 
    acceleration to determine the forward direction. After
    the forward direction is determined it will wait for 30
    seconds initially then dwell until a forward acceleration
    is detected. Once forward acceleration is detected the
    pod monitors the acceleration until it detects a negative
    acceleration. The pod waits for 2 seconds after negative
    acceleration is detected and then applies the brakes
    based on speed and distance.

'''


