#!/bash/python

import smbus
import time

##Serial.begin(115200)
# for RPI version 1, use 'bus = smbus.SMBus(0)'

bus = smbus.SMBus(1)

# This is the address set up on the Arduino
addr = 0x42
# add2 = 0x05

def writeNumber(value,device):
    # if device == 1:
    #     address = add1
    # else:
    #     if device == 2:
    #         address = add2
        
    bus.write_byte(device,value)
    # bus.write_byte_data(address,0,value)
    return -1

def readNumber(device):
    # if device == 1:
    #     address = add1
    # else:
    #     if device == 2:
    #         address = add2
    
    #number = bus.read_byte(address)
    # number = bus.read_byte_data(address,1)
    #return number


    #bus.read_i2c_block_data(address, command, count)
    #address:  The device (arduino) to connect to.  Set on the Arduino
    #command:  Hex value represents a register address on the Arduino
    #count:    Hex value for number of bits to expect from Command.
    val = bus.read_block_data(device, 0x02, 8)
    return val

# while True:
#     device = input("Choose an Arduino 1 or 2:")
#     var = input("Enter 1 - 9:")
#     if not var:
#         continue

#     writeNumber(var,device)
#     print "RPI: Hi Arduino, I sent you ", var
#     # sleep one second
#     time.sleep(1)

#     number=readNumber(device)
#     print "Arduino: Hey RPI, I received a digit ", number
    
