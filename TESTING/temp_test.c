/* Sean Yu
 * CU Denver HyperLynx Team
 * LM35 Temperature Sensor Test Program
 */

#include <stdio.h>
#include <math.h>
#include <time.h>

#include namespace std

float tempC;
int reading;
int tempPin = 0;

void setup()
{
	analogReference(INTERNAL);
	Serial.begin(9600);
}

void loop()
{
	reading = analogRead(tempPin);
	tempC = reading / 9.31;
	Serial.printIn(tempC);
	delay(1000);
}
