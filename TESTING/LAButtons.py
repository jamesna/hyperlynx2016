# Test script to make GUI buttons to control linear
# actuator(s) for Hyperlynx project
# Written by Nathan James
# 10/8/16


import signal
import time
import sys
from LinearActuator import LinearActuator
from PyMata.pymata import PyMata
from Tkinter import *
import ttk

####################################################
# Creating instance of PyMata for Arduino control
board = PyMata("/dev/ttyACM0", verbose=True)

def signal_handler(sig, frame):
    print('You pressed Ctrl+C')
    if board is not None:
        board.reset()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

forward = 5
reverse = 6

board.set_pin_mode(forward, board.PWM, board.DIGITAL)
time.sleep(2)
board.set_pin_mode(reverse, board.PWM, board.DIGITAL)
time.sleep(2)
board.set_pin_mode(9, board.PWM, board.DIGITAL)
time.sleep(2)
board.set_pin_mode(10, board.PWM, board.DIGITAL)
time.sleep(2)
board.set_pin_mode(3, board.PWM, board.DIGITAL)
time.sleep(2)
board.set_pin_mode(11, board.PWM, board.DIGITAL)
time.sleep(2)


frontMotor = LinearActuator(board,forward,reverse)
rearMotor = LinearActuator(board,9,10)
brakes = LinearActuator(board,3,11)

def UP(spd):
    frontMotor.motorF(spd)
    frontMotor.motorR(0)
    rearMotor.motorF(spd)
    rearMotor.motorR(0)
    brakes.motorF(spd)
    brakes.motorR(0)
    time.sleep(0.5)

def DN(spd):
    frontMotor.motorF(0)
    frontMotor.motorR(spd)
    rearMotor.motorF(0)
    rearMotor.motorR(spd)
    brakes.motorF(0)
    brakes.motorR(spd)
    time.sleep(0.5)

def ST():
    frontMotor.motorF(0)
    frontMotor.motorR(0)
    rearMotor.motorF(0)
    rearMotor.motorR(0)
    brakes.motorF(0)
    brakes.motorR(0)
####################################################
####################################################



####################################################
# Creating GUI env and buttons
spdF = 200
spdR = 100

root = Tk()

mForward = ttk.Button(root, text = "Motor\nForward")
mForward.pack()
mForward.config(command = lambda: UP(spdF))
#mForward.invoke()

mReverse = ttk.Button(root, text = "Motor\nReverse")
mReverse.pack()
mReverse.config(command = lambda: DN(spdR))
#mReverse.invoke()

stop = ttk.Button(root, text = "STOP")
stop.pack()
stop.config(command = ST)
#stop.invoke()

####################################################
####################################################



####################################################
root.mainloop()

#frontMotor.motorF(250)
#time.sleep(5)
#frontMotor.stop()
#
#time.sleep(4)
#
#frontMotor.motorR(200)
#time.sleep(3)
#frontMotor.stop()



# Close PyMata when done
#board.close()
