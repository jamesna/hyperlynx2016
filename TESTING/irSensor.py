'''
    Filename:   irSensor
    AUthors:    John Gorzynski and Nathan James
    Date:       1/27/17
    
    Description:
        Class for working with infrared sensors. Tachos for the masses
        
'''

import smbus
import time




class IrSensor():
    def __init__(self,address, szRtrn):
        self.tachoBus = smbus.SMBus(1)
        self.addr = address
        self.szRtrn = szRtrn
    
    
    def writing(self, value):
        self.tachoBus.write_byte(self.addr, value)
        
    def reading(self):
        val = self.tachoBus.read_i2c_block_data(self.addr, 0x00, self.szRtrn)
        return val
        


if __name__ == "__main__":
    Bernie = IrSensor(0x42,8)
    
    while True:
        var = input("Enter 1 - 9:")
        if not var:
            continue
    
        Bernie.writing(var)
        print "RPI: Hi Arduino, I sent you ", var
        # sleep one second
        time.sleep(1)
    
        number=Bernie.reading()
        print "Arduino: Hey RPI, I received a digit ", number
        
