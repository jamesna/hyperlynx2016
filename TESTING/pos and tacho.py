#!/bash/python

import smbus
import time
pos = []
tacho = []
posPIN = 3
tachoPIN = 5

tsINIT = time.ctime()


    
void setup()
{
    pinMode(posPIN, INPUT)
    pinMode(tachoPIN. INPUT)
}
        #Create two lists, one for Position, one for Tachometer
        #Record the initial timestamp when the program starts:  time.ctime()
        
        #Declare Pin 3 as Input  -- This is our Position pin
        #Declare Pin 5 as Input -- This is our Tachometer pin
        

   def getPosition(self):
        tsCURR = time.ctime()
        elapsed = tsCURR - tsINIT
        pos.append(elapsed)
        
        print("The current elapsed time is : " + elapsed )
        #When this is called, record a timestamp in the list.  Subtract the initial timestamp so that we have the time elapsed.
        
        #output the timestamp using print so we can see the values.
        #Print a label with each list so we know which one we're seeing.
        
    