from time import sleep
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
pin = 3
pin2 = 5
GPIO.setup(pin, GPIO.IN)
GPIO.setup(pin2, GPIO.IN)

c3 = 0
c5 = 0

def callback(channel):
    global c3
    if GPIO.input(3) == True:
        print("RISING edge detected on pin 3")
    else:
        print("Incorrect Trigger")
        
    c3 += 1
    return c3
def callback2(channel):
    global c5
    if GPIO.input(5) == True:
        print("RISING edge detected on pin 5")
    else:
        print("Incorrect Trigger")

    c5 += 1
    return c5
    
#Put Interrupt for Pin 3 here
GPIO.add_event_detect(pin, GPIO.RISING, callback=callback)

#Put Interrupt for Pin 5 here
GPIO.add_event_detect(pin2, GPIO.RISING, callback=callback2)

#if GPIO.input(pin) == True:
    #print ("Pin 3 is HIGH")
while(1):
    stuff = "Tacho:" + str(c3) + "   Position:" + str(c5)
    print(stuff)
    sleep(1)



