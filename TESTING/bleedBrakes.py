'''
Filename:	bleedBrakes.py
Author:		Nathan James
Date:		10/27/16


Description:
	runs the brake motor back and forth pumping the brakes to
	get the air bubbles out of the brake lines

'''

import sys
import signal
import time
from LinearActuator import LinearActuator
from PyMata.pymata import PyMata
from Tkinter import *
import ttk

####################################################
# Creating instance of PyMata for Arduino control
board = PyMata("/dev/ttyACM0", verbose=True)

#def signal_handler(sig, frame):
#    print('You pressed Ctrl+C')
#    if board is not None:
#        board.reset()
#    sys.exit(0)
#
#signal.signal(signal.SIGINT, signal_handler)

forward = 3
reverse = 11

board.set_pin_mode(forward, board.PWM, board.DIGITAL)
time.sleep(2)
board.set_pin_mode(reverse, board.PWM, board.DIGITAL)
time.sleep(2)


masterBrake = LinearActuator(board,forward,reverse)

def brakeBleed():
	print("bleed brakes Bleed")
	while True:
		masterBrake.motorF(50)
		masterBrake.motorR(0)
		time.sleep(6.1)
		masterBrake.motorF(0)
		masterBrake.motorR(50)
		time.sleep(4.4)
		#masterBrake.motorR(0)
		#time.sleep(3)
		#masterBrake.motorF(0)
		#masterBrake.motorR(50)
		#time.sleep(3)

def pare():
	masterBrake.motorF(0)
	masterBrake.motorR(50)
	time.sleep(6)
	masterBrake.motorR(50)
	print("STOPPED")


####################################################
# Creating GUI env and buttons
spdF = 50
spdR = 50

root = Tk()

mForward = ttk.Button(root, text = "Brake\nBleeder")
mForward.pack()
mForward.config(command = brakeBleed)
#mForward.invoke()

#mReverse = ttk.Button(root, text = "Motor\nReverse")
#mReverse.pack()
#mReverse.config(command = lambda: DN(spdR))
#mReverse.invoke()

stop = ttk.Button(root, text = "STOP")
stop.pack()
stop.config(command = pare)
#stop.invoke()

####################################################
####################################################

root.mainloop()
