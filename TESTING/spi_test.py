#!/usr/bin/env python

# File:     spi_test.py
# Project:  HyperLynx2016
# Author:   John Gorzynski
# Date:     1/21/2017

# Example from:  http://raspberrypi.stackexchange.com/questions/44277/spi-raspberry-pi-master-and-arduino-slave

#import time

#import pigpio # http://abyz.co.uk/rpi/pigpio/python.html

#pi = pigpio.pi('localhost',8888)

#if not pi.connected:
#    exit(0)

#h = pi.spi_open(0,40000) #spi_open( spi_channel, baud_rate, spi_flags)                        

#stop = 120.0

#n = 0

#while n < stop:

#    n += 1
#    pi.spi_xfer(h, "This is message number {}\n".format(n))
#    time.sleep(1)

#pi.spi_close(h)

#pi.stop()

import time
import spidev

resp = []

spi = spidev.SpiDev()
spidev.bits_per_word = 8
spidev.max_speed_hz = 115200  #Increase to 4000000 once working correctly
spi.open(0,0)

#start = spi.xfer2([0x02]) 
start = spi.xfer2([0xAA, 0x00])  #Send 0xAA to the Arduino to start sensor sampling
time.sleep(1) #Allow a delay before requesting measurements, for testing purposes

#for num in range(1,10):
resp = [1 , spi.xfer2([0x05, 0x03])]
time.sleep(0.5)
print resp
resp = [3 , spi.xfer2([0x05, 0x03])]
time.sleep(0.5)
print resp
resp = [4 , spi.xfer2([0x05, 0x03])]
time.sleep(0.5)
print resp
resp = [7 , spi.xfer2([0x05, 0x03])]
time.sleep(0.5)
#stuff = BytesToHex(resp)
print resp
time.sleep(0.01)  #time in seconds
    
#except KeyboardInterrupt:
spi.close()


#def BytesToHex(Bytes):
#    return ''.join(["0x02X " % x for x in Bytes]).strip()

