import time
import pyfirmata

board =  pyfirmata.Arduino('/dev/ttyACM0')

iter8 = pyfirmata.util.Iterator(board)
iter8.start()

LED = board.get_pin('d:13:o')
LED2 = board.get_pin('d:6:p')
DELAY = 500.0
STEPS = 10000.0

if __name__ == '__main__':
    for i in range(int(STEPS)):
		print i
		LED2.write(i / STEPS)
		#LED.write(1)
		time.sleep(0.001)
        #LED.write(0)
        #time.sleep(0.001)
    LED2.write(0)
    exit
