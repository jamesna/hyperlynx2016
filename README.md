# Hyperlynx Hyperloop pod project:  
## README ##



### What is this repository for? ###

The CU Denver Hyperlynx engineering team is designing a pod to test in the SpaceX hyperloop competition in January 2017. Various computer controlled systems allow sensor data collection and motor output based on pre-defined action for given situations.

Currently utilizing a Raspberry Pi for taking sensor input and main program execution and Arduino Uno for motor signal output to motor controllers. The main challenge is getting the Raspberry Pi to interact with the Arduino while being able to utilize pre-existing libraries written for Arduino Uno sketches.

Originally started with Pyfirmata, however it seems support and regular updates have stopped. PyMata seems to be promising with regular updates and a tutorial detailing how to add your own device support from pre-existing libraries.


* Version 0.1.0


### How do I get set up? ###
Running the `$ . pythonsetup.sh` bash script should setup everything on the Raspberry Pi for using this repo. The following things are what this script installs:

To install extra python packages we need to make sure python package manager (pip) is installed. To do this run in bash:
`$ sudo python get-pip.py`

#### PyMata ####
PyMata seems to be the best, most up-to-date framework for working through Arduino's Firmata protocol. The communication is asynchronous consisting of command calls and call for responses. To install from bash:
`$ sudo pip install pymata`

#### Adafruit_Python_GPIO ####
Temperapute and gas pressure sensor BMP180 uses the Adafruit I2C library

There is a newer version of pymata, however it utilizes python 3.5.1 but our raspberry pi is still using python 2.7.6 so the older version is currently used.

* Configuration
    * Arduino Pin Configuration
        * Front DFA motor: extend = 5, retract = 6, position = A3
        * Rear DFA motor: extend = 9, retract = 10, position = A4
        * Brake motor: extend = 3, retract = 11, position = A5
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

#### Modules ####
The modules folder contains the specific modules written to control the different parts of the pod's sensor's and motors.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Questions contact Nathan (nathan.james@ucdenver.edu)
* Other community or team contact